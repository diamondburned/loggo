package main

import (
	"bufio"
	"flag"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"
)

var (
	path = ""
	exSw = false
	args = ""
	deli = ""
)

func flags() {
	flag.StringVar(&path, "D", "",
		"Destination of the log file (default \"/tmp/RFC3339.log\")")

	flag.BoolVar(&exSw, "e", false,
		"Executes command instead of taking stdin")

	flag.StringVar(&deli, "d", " - ",
		"Delimiter to separate the prefix with the content")

	flag.Parse()

	args = strings.Join(flag.Args(), "")
}

func main() {
	flags()

	if path == "" {
		path = "/tmp/" + time.Now().Format("2006.01.02.15.04.05") + ".log"
	}

	file, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_SYNC|os.O_APPEND, os.ModePerm)
	if err != nil {
		exitCleanly(file, err)
	}

	defer func() {
		if err := file.Close(); err != nil {
			exitCleanly(file, err)
		}
	}()

	if !exSw {
		bufout := bufio.NewScanner(os.Stdin)
		bufout.Split(bufio.ScanLines)

		for bufout.Scan() {
			prefix := []byte("\n" + time.Now().Format(time.RFC3339Nano) + deli)

			if _, err := file.Write(append(prefix, bufout.Bytes()...)); err != nil {
				exitCleanly(file, err)
			}
		}

		if err := bufout.Err(); err != nil {
			exitCleanly(file, err)
		}
	} else {
		cmd := exec.Command("sh", "-c", args)

		stdout, err := cmd.StdoutPipe()
		if err != nil {
			exitCleanly(file, err)
		}

		stderr, err := cmd.StderrPipe()
		if err != nil {
			exitCleanly(file, err)
		}

		outscan := bufio.NewScanner(stdout)
		outscan.Split(bufio.ScanLines)

		errscan := bufio.NewScanner(stderr)
		errscan.Split(bufio.ScanLines)

		wg := &sync.WaitGroup{}

		if err := cmd.Start(); err != nil {
			exitCleanly(file, err)
		}

		wg.Add(1)
		go scanWrite(file, outscan, wg)

		wg.Add(1)
		go scanWrite(file, errscan, wg)

		if err := cmd.Wait(); err != nil {
			panic(err)
		}

		wg.Wait()

	}

	if _, err := file.Write([]byte("\n")); err != nil {
		exitCleanly(file, err)
	}
}

func scanWrite(file *os.File, scanner *bufio.Scanner, wg *sync.WaitGroup) {
	for scanner.Scan() {
		if _, err := file.Write(append([]byte("\n"+time.Now().Format(time.RFC3339)+" - "), scanner.Bytes()...)); err != nil {
			exitCleanly(file, err)
		}
	}

	wg.Done()
}

func exitCleanly(file *os.File, err error) {
	if file != nil {
		file.Close()
	}

	panic(err)
}
