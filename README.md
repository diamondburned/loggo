# loggo

Experimental logger for binaries

## Why?

Was debugging `osu-wine`, but the stdout had no timestamp.

## How to use?

1. [Get binary](https://gitlab.com/diamondburned/loggo#get-binary)
2. `command &> >(./loggo)` (bash-only, `&>` for stdout+err, `>` for stdout)

**or**

1. [Get binary](https://gitlab.com/diamondburned/loggo#get-binary)
1024. `./loggo -e "command"` (`command` is executed with `sh`)

## Other

`./loggo -h`

## Get binary

`go get -u gitlab.com/diamondburned/loggo`

**or**

grab latest artifact and unzip