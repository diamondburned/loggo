#!/bin/sh
if [ $1 = cat ]; then
	printf "%s\n" /tmp/"$(date +"%Y.%m.%d")"*.log | tail -n1
else
	cat "$(printf "%s\n" /tmp/"$(date +"%Y.%m.%d")"*.log | tail -n1)"
fi

